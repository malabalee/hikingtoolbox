<?php

// Identify user by cookie, ip address, Google or Facebook account
// Result is stored in $user as integer key

require_once('../resources/config.php');
include_once('include.php');

//echo var_dump($_COOKIE).BR;

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);

if ( mysqli_connect_errno() ) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$ckey = PREFIX.'-user-cookie';
$cookie = $_COOKIE[$ckey];
if ( isset($cookie) ) {
    if( $dev ) {
        echo $ckey.': '.$cookie.BR;
        //echo $HTTP_COOKIE_VARS[$ckey].BR;
    }
} else {
    $cookie = uniqid();
    if ( $dev ) echo 'new user: '.$cookie.BR;
    setcookie($ckey, $cookie, time()+DAY*180);
}

if ($stmt = $mysqli->prepare('SELECT id FROM '.PREFIX.'.user WHERE cookie=?')) {
    $stmt->bind_param('s', $cookie);
    $stmt->execute();
    $stmt->bind_result($user);
    $stmt->fetch();
    if ( isset($user) ) {
        if ( $dev ) printf('user %s identified by cookie'.BR, $user);
    }
    $stmt->close();
}

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
$ip = ip2long($ip);

if ( $dev ) {
    echo 'ip: '.long2ip($ip).BR;
    echo 'ip2long: '.$ip.BR;
    echo 'user: '.$user.BR;
}

if ( !isset($user) ) {
    if ( $stmt = $mysqli->prepare('INSERT INTO '.PREFIX.'.user (cookie, ip, google, facebook, flag) VALUES (?, ?, ?, ?, ?)') ) {
        $stmt->bind_param('sissi', $cookie, $ip, $google, $facebook, $flag);
        $google = '';
        $facebook = '';
        $flag = 0;
        if ( $stmt->execute() ) {
            if ( $dev ) {
                printf("%d Row inserted.".BR, $stmt->affected_rows);
            }
        } else if ( $dev ) {
            echo 'mysqli statement error: '.$stmt->error.BR;            
        }
        $stmt->close();
    } else {
        printf("Error: %d.\n", $mysqli->error);
    }
} else {
}

$mysqli->close();

?>