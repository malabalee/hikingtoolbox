<?php

require_once('../resources/config.php');
include_once('include.php');

$gpx = simplexml_load_file('test.gpx');

$glocinfo = '';
$gpxelev = [];

//echo var_dump($gpx).'<br />';
foreach ($gpx->wpt as $wpt) {
    //echo var_dump($wpt).'<br />';
}
foreach ($gpx->trk as $trk) {
    //echo var_dump($trk).'<br />';
    foreach ($trk->trkseg as $seg) {
        //echo var_dump($seg).'<br />';
        foreach ($seg->trkpt as $pt) {
            //echo var_dump($pt).'<br />';
            $lat = floatval( $pt['lat'] );
            $lon = floatval( $pt['lon'] );
            $time = $pt['time'];
            $name = $pt->name;
            $gpxelev[] = $pt->ele;
            //if ( !empty($glocinfo) ) $glocinfo .= LF;
            $glocinfo .= $lon.' '.$lat.LF;
        }
    }
}

$twDEM_path = '../resources/gis/twdtm_asterV2_30m.tif';
$cmd = sprintf("printf \"%s\n\" | gdallocationinfo -valonly -wgs84 %s", $glocinfo, $twDEM_path);
//echo $cmd.BR;
exec($cmd, $out, $ret);
if ( $ret == 0 ) {
    //echo var_dump( $out ).BR;
}

foreach( $gpxelev as $key => $value ) {
    echo $value.', '.$out[$key].BR;
}

?>