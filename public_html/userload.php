<?php

// Load user data

require_once('../resources/config.php');
include_once('include.php');
@include_once('id_user.php');

$target_dir = sys_get_temp_dir();

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);
if ( mysqli_connect_errno() ) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$ar = [];
$ar['error_code'] = 0;

if ($stmt = $mysqli->prepare('SELECT * FROM '.PREFIX.'.file WHERE owner=? ORDER BY timeupdate DESC LIMIT 8')) {
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $result = $stmt->get_result();
    while ( $row = $result->fetch_array(MYSQLI_NUM) )
    {
        $fileid     = $row[0];
        $user       = $row[1];
        $md5        = $row[2];
        $type       = $row[3];
        $mime       = $row[4];
        $filepath   = $row[5];
        $filename   = $row[6];
        
        // All information not loaded from database should be enclosed in an array
        // This is still a bad practice as database structure may be altered
        // Need better handling in the future
        $fediting   = $filepath.'.'.$user.'.tmp';
        $fophx      = $filepath.'.'.$user.'.ophx';
        $row[]      = array(
            'editing'   => file_exists($fediting),
            'tmp'       => $fediting,
            'ophx'      => $fophx
        );
        
        $ar['filehx'][] = $row;
        if ( $dev ) {
            foreach ($row as $key => $value)
            {
                echo $key.': '.$value.BR;
            }
            echo BR;
        }
    }
    $stmt->close();
}

if ( !array_key_exists('filehx', $ar) ) $ar['error_code'] = 1;

echo json_encode( $ar );

?>