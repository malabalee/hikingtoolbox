<?php

require_once('../resources/config.php');
include_once('include.php');

$post   = file_get_contents( 'php://input' );

$json   = json_decode( $post, true );

$debug = '';
//$debug = var_dump( $json );

$fileid     = $json['source'][0];
$user       = $json['source'][1];
$type       = $json['source'][3];
$mime       = $json['source'][4];
$filepath   = $json['source'][5];
$filename   = $json['source'][6];

file_put_contents ( $filepath.'.'.$user.'.ophx', $post );

$ddsource = new DOMDocument(); // DOMDocument of source gpx; not to be modified
$ddsource->load( $filepath );
$xmlsource = $ddsource->saveXML();
$nltrkpt = $ddsource->getElementsByTagName('trkpt'); // DOMNodeList of trkpt

$ddedit = new DOMDocument(); // DOMDocument for editing
$ddedit->loadXML( $xmlsource );

$remove = [];
$nodelist = $ddedit->getElementsByTagName('trkseg'); // DOMNodeList of trkpt
foreach ( $nodelist as $node) {
    $remove[] = $node;
}
foreach ( $remove as $node) {
    $node->parentNode->removeChild( $node );
}

//$debug .= printf( "nltrkpt: %d, ", count($nltrkpt) );
$trk = $ddedit->getElementsByTagName('trk')->item(0);
foreach ( $json['seglist'] as $eseg ) {
    //$debug .= var_dump($eseg).', ';
    $type   = $eseg[2];
    $trkseg = null;
    if ( $type!='d' ) {
        $begin  = $eseg[0];
        $end    = $eseg[1];
        $dir    = 1;
        $offset = 0;
        //$debug .= printf( "(%d, %d), ", $begin, $end );
        if ( $begin > $end ) {
            $dir    = -1;
            $tmp    = $begin;
            $begin  = $end;
            $end    = $tmp;
            $offset = $begin + $end;
        }
        if ( $trkseg==null ) $trkseg = $ddedit->createElement('trkseg');
        for ( $i=$begin; $i<=$end; $i++) {
            if ( $dir > 0 ) {
                $node = $ddedit->importNode( $nltrkpt->item($i), true );
            } else {
                $node = $ddedit->importNode( $nltrkpt->item( $offset - $i ), true );
            }
            $trkseg->appendChild( $node );
        }
    }
    if ( $trkseg!=null ) $trk->appendChild($trkseg);
}

$debug .= strlen( $ddsource->saveXML() ).', ';
$debug .= strlen( $ddedit->saveXML() ).', ';

$dest = $filepath.'.'.$user.'.tmp';
$bytes = $ddedit->save( $dest );
//chmod($dest, 0644);

$debug .= $bytes.', ';
$debug .= substr(sprintf('%o', fileperms($dest)), -4);

foreach ( $json['ophx'] as $op ) {
    $operation   = $op[0];
    $param       = $op[1];
    //$debug .= $operation[0].': '.$operation[1].', ';
}

/*$md5 = md5_file($dest);

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);
if ( mysqli_connect_errno() ) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
if ($stmt = $mysqli->prepare('SELECT id FROM '.PREFIX.'.file WHERE path=? AND owner=?')) {
    $stmt->bind_param('si', $dest, $user);
    $stmt->execute();
    $stmt->bind_result($fexisting);
    $stmt->fetch();
    $stmt->close();
}
if ( isset($fexisting) ) {
    // Update existing file
    //$debug = 'db entry exist';
} else {
    if ( $stmt = $mysqli->prepare('INSERT INTO '.PREFIX.'.file (owner, md5, type, mime, path, name, timecreate, timeupdate, flag) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)') ) {
        $stmt->bind_param('isssssiii', $user, $md5, $type, $mime, $dest, $filename, $timecreate, $timeupdate, $flag);
        $type = 'gpx_tmp';
        $timecreate = time();
        $timeupdate = time();
        $flag = 0;
        $stmt->execute();
        $stmt->close();
    } else {
        printf("Error: %d.\n", $mysqli->error);
        exit();
    }
}
$mysqli->close();*/

echo json_encode(array(
    'error_code'    =>  0,
    'filename'      =>  $filename,
    'source'        =>  $filepath,
    'dest'          =>  $dest,
    'debug'         =>  $debug
));

?>