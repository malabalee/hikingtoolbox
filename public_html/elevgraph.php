<?php

require_once('../resources/config.php');
include_once('include.php');

@$fname = urldecode ($_GET['f']);
@$width = (int)$_GET['w'];
@$height = (int)$_GET['h'];
if ( empty($fname) ) $fname = 'test.gpx'; 
if ( empty($width) ) $width = 1920; 
if ( empty($height) ) $height = 270; 

$gpx = simplexml_load_file( $fname );

$glocinfo = '';
$gpxelev = [];

//echo var_dump($gpx).'<br />';
foreach ($gpx->wpt as $wpt) {
    //echo var_dump($wpt).'<br />';
}
foreach ($gpx->trk as $trk) {
    //echo var_dump($trk).'<br />';
    foreach ($trk->trkseg as $seg) {
        //echo var_dump($seg).'<br />';
        foreach ($seg->trkpt as $pt) {
            //echo var_dump($pt).'<br />';
            $lat = floatval( $pt['lat'] );
            $lon = floatval( $pt['lon'] );
            $time = $pt['time'];
            $name = $pt->name;
            $gpxelev[] = $pt->ele;
            //if ( !empty($glocinfo) ) $glocinfo .= LF;
            $glocinfo .= $lon.' '.$lat.LF;
        }
    }
}

$twDEM_path = '../resources/gis/twdtm_asterV2_30m.tif';
$cmd = sprintf("printf \"%s\n\" | gdallocationinfo -valonly -wgs84 %s", $glocinfo, $twDEM_path);
//echo $cmd.BR;
exec($cmd, $out, $ret);
if ( $ret == 0 ) {
    //echo var_dump( $out ).BR;
}

$elevmax = -9999;
$elevmin = 9999;
foreach( $gpxelev as $key => $value ) {
    if ( (int)$value > $elevmax ) $elevmax = (int)$value;
    if ( (int)$value < $elevmin ) $elevmin = (int)$value;
}
$elevdiff = $elevmax - $elevmin;
$elevmax += $elevdiff/4;
$elevmin -= $elevdiff/4;
$elevdiff = $elevmax - $elevmin;
$scalev = $height / $elevdiff;

if ( $width > count($gpxelev) ) $width = (int)count($gpxelev);
$scaleh = (float)count($gpxelev) / $width;
//echo count($gpxelev).' / '.$width.' = '.$scaleh.BR;

if ( empty(ob_get_contents()) ) {
    header("Content-type: image/jpeg");
    $im     = imagecreatetruecolor($width, $height)
              or die('Cannot Initialize new GD image stream');
    $background = imagecolorallocate($im, 0, 0, 0);
    $ground = imagecolorallocate($im, 64, 255, 64);
    imagefilledrectangle($im, 0, 0, $width-1, $height-1, $background);
    for ($x = 0; $x < $width; $x++) {
        //echo $value.', '.$out[$key].BR;
        $e = ($out[(int)($x*$scaleh)] - $elevmin) * $scalev;
        $y = (int)round($height-$e);
        //echo $key.' - '.$y.BR;
        imageline( $im, $x, $y, $x, $height-1, $ground );
    }
    imagejpeg( $im );
    imagedestroy( $im );
}

?>