<?php

require_once('../resources/config.php');
include_once('include.php');

$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

// ********** Important Note!!! **********
// New column must be added to the end of the table or array index will be messed up!

mysqli_query($link, 'CREATE TABLE IF NOT EXISTS '.PREFIX.'.user (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    cookie TEXT,
    ip INT UNSIGNED,
    google TEXT,
    facebook TEXT,
    flag INT UNSIGNED
)');

mysqli_query($link, 'CREATE TABLE IF NOT EXISTS '.PREFIX.'.file (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    owner INT,
    md5 TEXT,
    type TEXT,
    mime TEXT,
    path TEXT,
    name TEXT,
    timecreate INT UNSIGNED,
    timeupdate INT UNSIGNED,
    flag INT
)');

$sql = 'SHOW TABLES FROM '.PREFIX;
$result = mysqli_query($link, $sql);

if (!$result) {
    echo 'DB Error, could not list tables'.BR;
    echo 'MySQL Error: ' . mysql_error();
    exit;
}

echo '<ul>';
while ( $row = $result->fetch_row() ) {
    echo '<li>TABLE '.$row[0].'</li>';
    $sql = 'DESCRIBE '.PREFIX.'.'.$row[0];
    $describe = mysqli_query($link, $sql);
    echo '<ul>';
    while ( $col = $describe->fetch_row() ) {
        echo '<li>'.$col[0].'</li>';
    }
    echo '</ul>';
}
echo '</ul>';

mysqli_close($link);

?>