<?php

define("NL", "\r\n");
define("BR", "<br />");
define("LF", "\\n");

define("HOUR", 3600);
define("DAY", 3600 * 24);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dev = true;
/* AJAX check  */
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $dev = false;
}

?>