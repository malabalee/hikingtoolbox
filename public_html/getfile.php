<?php

$name = urldecode ($_GET['n']);
$file = urldecode ($_GET['f']);
if ( file_exists($file) ) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($name).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
} else {
    header( 'HTTP/1.0 404 Not Found' );
}

?>