<?php

require_once('../resources/config.php');
include_once('include.php');
@include_once('id_user.php');

$target_dir = sys_get_temp_dir();
//echo var_dump($_FILES).NL;
//echo $target_dir.NL;
    
    /*["name"]=>
    string(10) "haishu.jpg"
    ["type"]=>
    string(10) "image/jpeg"
    ["tmp_name"]=>
    string(14) "/tmp/phpyN13RV"
    ["error"]=>
    int(0)
    ["size"]=>
    int(161971)*/
    
if( $_FILES['file']['error'] === UPLOAD_ERR_OK ) {
    $tmpfile = $_FILES['file']['tmp_name'];
    $mime = mime_content_type($tmpfile);
    if ( $mime === 'application/xml' ) {
        @$xml = simplexml_load_file($tmpfile);
        if ( $xml ) {
            //echo $xml.NL;
            $md5 = md5_file($tmpfile);
            //echo 'md5: '.$md5.NL;
            $path = PATH_UPLOADS.'/'.substr($md5, 0, 2).'/'.substr($md5, 2, 2).'/'.substr($md5, 4, 2);
            @mkdir($path, 0777, true);
            move_uploaded_file($tmpfile, $path.'/'.$md5);
            //echo 'owner: '.$user.NL;

            $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);
            if ( mysqli_connect_errno() ) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            if ($stmt = $mysqli->prepare('SELECT id FROM '.PREFIX.'.file WHERE md5=? AND owner=?')) {
                $stmt->bind_param('si', $md5, $user);
                $stmt->execute();
                $stmt->bind_result($fexisting);
                $stmt->fetch();
                $stmt->close();
            }
            if ( isset($fexisting) ) {
                // Update existing file
            } else {
                if ( $stmt = $mysqli->prepare('INSERT INTO '.PREFIX.'.file (owner, md5, type, mime, path, name, timecreate, timeupdate, flag) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)') ) {
                    $stmt->bind_param('isssssiii', $user, $md5, $type, $mime, $filepath, $_FILES['file']['name'], $timecreate, $timeupdate, $flag);
                    $type = 'gpx_upload';
                    $filepath = $path.'/'.$md5;
                    $timecreate = time();
                    $timeupdate = time();
                    $flag = 0;
                    $stmt->execute();
                    //printf("%d Row inserted.".BR, $stmt->affected_rows);
                    $stmt->close();
                } else {
                    printf("Error: %d.\n", $mysqli->error);
                }
            }
            $mysqli->close();

            $gpx = simplexml_load_file($path.'/'.$md5);
            foreach ($gpx->trk as $trk) {
                foreach ($trk->trkseg as $seg) {
                    foreach ($seg->trkpt as $pt) {
                        $lat = floatval( $pt['lat'] );
                        $lon = floatval( $pt['lon'] );
                        $time = $pt->time;
                        $ele = $pt->ele;
                        $name = $pt->name;
                    }
                }
            }
            unset($gpx);
            
            echo json_encode(array(
                'file'          =>  $path.'/'.$md5,
                'name'          =>  $_FILES['file']['name'],
                'error_code'    =>  0,
            ));
        } else {
            echo json_encode(array(
                'error_code'    =>  1,
                'error_message' =>  'Invalid GPX file. simplexml_load_file() failed.'
            ));
        }
    } else {
        echo json_encode(array(
            'error_code'    =>  1,
            'error_message' =>  'Invalid GPX file. mime_content_type mismatch: '.mime_content_type($tmpfile)
        ));
    }
} else {
    echo 'PHP file_uploads error: '.$_FILES['file']['error'].NL;
    echo 'http://php.net/manual/en/features.file-upload.errors.php';
}

?>